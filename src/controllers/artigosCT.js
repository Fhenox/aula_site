module.exports.enviarArtigo = function(app, req, res){
    var dadosArtigo = req.body

    var connection = app.config.dbConnNOSQL

    var ArtigosModels = new app.models.ArtigosModels(connection)

    ArtigosModels.inserirArtigos(dadosArtigo)

    res.send(dadosArtigo)
}