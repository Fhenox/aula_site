var index = require('../../database/index.json')
var usuarios = require('../../database/usuarios.json')
var artigos = require('../../database/artigos.json')
var dados = require('../../database/dados.json')

module.exports = function(app) {
    app.get('/', function(req, res){
        // console.log('index: ', index);
        res.render('index', {data: index})
    })
   
    app.get('/usuarios', function(req, res){
        // console.log('usuarios: ', usuarios);
        res.render('usuarios', {data: usuarios})
    })

    app.get('/artigos', function(req, res){
        // console.log('artigos: ', artigos);
        res.render('artigos', {data: artigos})
    })
    
    app.get('/dados', function(req, res){
        // console.log('artigos: ', artigos);
        res.send({dados})
    })
}