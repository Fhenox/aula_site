function ArtigosModels(connection) {
    this._connection = connection();
}

ArtigosModels.prototype.inserirArtigos = function (dados) {
    this._connection.open(function (err, mongoClient) {
        mongoClient.connection('artigos', function (err, collection) {
            collection.insert(dados)
            mongoClient.close()

        })
    })
}

module.exports = function(){
    return ArtigosModels
}